from math import *

characterName = "lorem ipsum"
print(characterName)
print(characterName.upper())

print(3 * (4 + 5))
print(10 % 3)

myNum = 5
print(str(myNum) + " lorem")
# print(str(myNum))

print(max(1, 2, 3))

print(floor(3.4))

variable = input("give some!")
print("you gave some: " + variable)

val1 = input("Num1: ")
val2 = input("Num2: ")

val3 = float(val1) + int(val2)
print(val3)
